package com.example.memorygame.model

import com.example.memorygame.R

val CARDS = arrayOf(
    setOf( // EASY
        Card(R.raw.e1), Card(R.raw.e1),
        Card(R.raw.e2), Card(R.raw.e2),
        Card(R.raw.e3), Card(R.raw.e3),
        Card(R.raw.e4), Card(R.raw.e4),
        Card(R.raw.e5), Card(R.raw.e5),
        Card(R.raw.e6), Card(R.raw.e6)
    ),
    setOf( // MEDIUM
        Card(R.raw.m1), Card(R.raw.m1),
        Card(R.raw.m2), Card(R.raw.m2),
        Card(R.raw.m3), Card(R.raw.m3),
        Card(R.raw.m4), Card(R.raw.m4),
        Card(R.raw.m5), Card(R.raw.m5),
        Card(R.raw.m6), Card(R.raw.m6)
    ),
    setOf( // HARD
        Card(R.raw.h1), Card(R.raw.h1),
        Card(R.raw.h2), Card(R.raw.h2),
        Card(R.raw.h3), Card(R.raw.h3),
        Card(R.raw.h4), Card(R.raw.h4),
        Card(R.raw.h5), Card(R.raw.h5),
        Card(R.raw.h6), Card(R.raw.h6)
    )
)

class Card(var drawable: Int) {

    var revealed : Boolean = false
    private var locked : Boolean = false

    fun lock() {
        this.locked = true
    }

    fun flip() : Boolean {
        if (locked) return false
        revealed = !revealed
        return true
    }

    fun reset() {
        revealed = false
        locked = false
    }

}