package com.example.memorygame.viewmodel

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.memorygame.model.CARDS
import com.example.memorygame.model.Card

class CardsViewModel : ViewModel() {

    val deck = MutableLiveData<List<Card>>().apply {
        this.value = listOf()
    }
    val score = MutableLiveData<Int>()
    val finish = MutableLiveData<Boolean>()

    private lateinit var shuffledDecks : Array<List<Card>>

    private fun timer() {
        Handler(Looper.getMainLooper()).postDelayed(::timer, 1000)
        if (gameLock) return
        score.postValue(score.value?.plus(-1))
    }

    init {
        restart()
    }

    fun restart() {
        shuffledDecks = arrayOf(
            CARDS[0].shuffled(),
            CARDS[1].shuffled(),
            CARDS[2].shuffled()
        )
        // Treu 1 punt del score cada segon
        Handler(Looper.getMainLooper()).postDelayed(::timer, 1000)
        score.value = 100
        finish.value = false
        // -_-
        for (deck in shuffledDecks)
            for (card in deck)
                card.reset()
    }

    private var lastMove : Card? = null // Guarda la anterior carta girada
    var gameLock : Boolean = false // Para el timeout y el pawse [sic]
    fun move(pos : Int) : Boolean {
        val c = deck.value!![pos]
        if (gameLock || c.revealed || !c.flip()) return false
        if (lastMove != null) {
            if (c.drawable == lastMove!!.drawable) {
                score.postValue(score.value?.plus(10))
                lastMove!!.lock()
                c.lock()
            } else {
                score.postValue(score.value?.minus(5))
                val pivot = lastMove!!
                gameLock = true
                Handler(Looper.getMainLooper()).postDelayed({
                    gameLock = false
                    pivot.flip()
                    deck.value!![pos].flip()
                    deck.value = deck.value
                }, 750)
            }
            lastMove = null
        } else lastMove = c
        deck.value = deck.value
        return true
    }

    fun setDifficulty(difficulty : String) {
        deck.value =
            when(difficulty) {
                "easy"   -> shuffledDecks[0]
                "medium" -> shuffledDecks[1]
                "hard"   -> shuffledDecks[2]
                else     -> shuffledDecks[0]
            }
    }

}