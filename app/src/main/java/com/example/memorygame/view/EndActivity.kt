package com.example.memorygame.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.memorygame.R
import com.example.memorygame.databinding.ActivityEndBinding

class EndActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.Theme_MemoryGame)
        super.onCreate(savedInstanceState)
        val bind = ActivityEndBinding.inflate(layoutInflater)
        setContentView(bind.root)

        val score = intent.getIntExtra("score", 0).toString()
        val difficulty = intent.getStringExtra("difficulty")
        bind.score.text = score

        bind.btnContinue.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }

        bind.btnRepeat.setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)
            intent.putExtra("difficulty", difficulty)
            startActivity(intent)
            finishAffinity()
        }

        bind.btnShare.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, getText(R.string.share_msg).toString() + score)
            startActivity(intent)
        }

    }

}