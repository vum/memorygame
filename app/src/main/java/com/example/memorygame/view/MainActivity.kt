package com.example.memorygame.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.activity.viewModels
import com.example.memorygame.R
import com.example.memorygame.databinding.ActivityMainBinding
import com.example.memorygame.viewmodel.CardsViewModel

class MainActivity : AppCompatActivity() {

    private val cardsViewModel : CardsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.Theme_MemoryGame)
        super.onCreate(savedInstanceState)
        val bind = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bind.root)

        // SPLASH SCREEN: https://developer.android.com/guide/topics/ui/splash-screen ;-;
        bind.root.viewTreeObserver.addOnPreDrawListener {
            Thread.sleep(500)
            return@addOnPreDrawListener true
        }


        // SPINER DE DIFICULTAT
        var difficulty = "easy"
        bind.difficulty.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                val difficulties = arrayOf("easy", "medium", "hard")
                difficulty = difficulties[position]
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
                difficulty = "easy"
            }
        }

        // COMENÇAR JOC
        bind.play.setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)
            intent.putExtra("difficulty", difficulty)
            cardsViewModel.restart() // TODO aaaaaaaa
            startActivity(intent)
        }

    }

}