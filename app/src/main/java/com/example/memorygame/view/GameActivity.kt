package com.example.memorygame.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import android.widget.ImageView
import androidx.activity.addCallback
import androidx.activity.viewModels
import androidx.core.view.children
import com.example.memorygame.R
import com.example.memorygame.databinding.ActivityGameBinding
import com.example.memorygame.viewmodel.CardsViewModel
import kotlin.collections.ArrayList

class GameActivity : AppCompatActivity() {

    private lateinit var bind : ActivityGameBinding
    private val cardsViewModel : CardsViewModel by viewModels()
    private var images : MutableList<ImageView> = ArrayList()


    override fun onCreate( savedInstanceState: Bundle? ) {

        setTheme(R.style.Theme_MemoryGame)
        super.onCreate(savedInstanceState)
        bind = ActivityGameBinding.inflate(layoutInflater)
        setContentView(bind.root)


        // El mall de cartes per dificultat remenat
        val difficulty = intent.getStringExtra("difficulty").toString()
        cardsViewModel.setDifficulty(difficulty)

        for (row in bind.board.children) {
            for (image in (row as ViewGroup).children) {
                images.add(image as ImageView)
            }
        }

        for (i in images.indices) {
            val image = images[i]
            image.setOnClickListener {
                cardsViewModel.move(i)
            }
        }


        // EL back button va al MainActivity sempre
        this.onBackPressedDispatcher.addCallback(this) {
            val intent = Intent(this@GameActivity, MainActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }



        // ------------- OBSERVERS -------------

        cardsViewModel.deck.observe(this, {
            for (i in images.indices) {
                images[i].setImageResource(
                    if (it[i].revealed) it[i].drawable
                    else R.drawable.back
                )
            }
            for (c in it)
                if (!c.revealed) return@observe
            cardsViewModel.finish.postValue(true)
        })

        cardsViewModel.score.observe(this, {
            bind.score.text = it.toString()
        })

        cardsViewModel.finish.observe(this, {
            if (it) {
                val intent = Intent(this, EndActivity::class.java)
                intent.putExtra("score", cardsViewModel.score.value)
                intent.putExtra("difficulty", difficulty)
                startActivity(intent)
                cardsViewModel.gameLock = true
                cardsViewModel.restart()
            }
        })

    }

}